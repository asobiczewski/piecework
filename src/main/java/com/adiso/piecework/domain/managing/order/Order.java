package com.adiso.piecework.domain.managing.order;

import com.adiso.piecework.domain.managing.part.Part;
import com.adiso.piecework.web.rest.dto.OrderDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "ORDERS")
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ORDER_ID")
    private Long id;

    @NonNull
    @Column(name = "NAME")
    private String name;

    @OneToMany(mappedBy = "")
    private List<Part> parts = new ArrayList<>();

    public static Order of(OrderDto dto) {
        Order order = new Order(dto.getName());
        order.setId(Long.valueOf(dto.getId()));
        order.setParts(dto.getParts().stream()
            .map(Part::of)
            .collect(Collectors.toList())
        );
        return order;
    }
}
