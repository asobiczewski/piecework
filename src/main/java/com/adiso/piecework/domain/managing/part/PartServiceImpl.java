package com.adiso.piecework.domain.managing.part;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Log
public class PartServiceImpl implements PartService {

    private final PartRepository repository;

    @Autowired
    public PartServiceImpl(PartRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Part> getAll() {
        return repository.findAll();
    }

    @Override
    public void create(Part part) {
        repository.save(part);
        log.info("Created Part: " + part.getName());
    }

    @Override
    public void update(Part part) {
        repository.save(part);
        log.info("Updated Part: " + part.getName());
    }

}
