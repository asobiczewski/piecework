package com.adiso.piecework.domain.managing.part;

import com.adiso.piecework.web.rest.dto.PartDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "PARTS")
public class Part implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "PART_ID")
    private Long id;

    @Column(name = "NAME", unique = true)
    @NonNull
    private String name;

    @Column(name = "PRICE")
    private BigDecimal price;

    public static Part of(PartDto dto) {
        Part part = new Part(dto.getName());
        if (dto.hasId()) {
            part.setId(Long.valueOf(dto.getId()));
        }
        if (dto.hasPrice()) {
            part.setPrice(new BigDecimal(dto.getPrice()));
        }
        return part;
    }

    public String getPriceString() {
        return price == null ? "" : price.toString();
    }

}
