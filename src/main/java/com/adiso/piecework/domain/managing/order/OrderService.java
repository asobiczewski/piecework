package com.adiso.piecework.domain.managing.order;

import java.util.List;

public interface OrderService {
    List<Order> getAll();

    void create(Order order);
}
