package com.adiso.piecework.domain.managing.part;

import org.springframework.data.jpa.repository.JpaRepository;

interface PartRepository extends JpaRepository<Part, Long> {

}
