package com.adiso.piecework.domain.managing.part;

import java.util.List;

public interface PartService {
    List<Part> getAll();

    void create(Part part);

    void update(Part part);
}
