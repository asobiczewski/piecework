package com.adiso.piecework.domain.planning;

import com.adiso.piecework.domain.managing.order.Order;
import lombok.Value;

import java.util.List;

@Value
public class OrderPlan {
    private Order order;
    private List<PartPlan> partPlans;
}
