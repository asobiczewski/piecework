package com.adiso.piecework.domain.planning;

import com.adiso.piecework.domain.managing.part.Part;
import lombok.Value;

@Value
public class PartPlan {
    private Part part;
    private int quantity;
}
