package com.adiso.piecework.domain.planning;

import lombok.Data;
import lombok.NonNull;

import javax.persistence.Id;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
public class WorkPlan {

    @Id
    @NonNull
    private LocalDate date;
    private List<OrderPlan> orderPlans = new ArrayList<>();

    public WorkPlan() {
        date = LocalDate.now();
    }
}
