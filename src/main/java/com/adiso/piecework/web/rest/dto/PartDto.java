package com.adiso.piecework.web.rest.dto;

import com.adiso.piecework.domain.managing.part.Part;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

import static org.apache.commons.lang.StringUtils.isNotEmpty;

@Data
@NoArgsConstructor
@XmlRootElement
public class PartDto implements Serializable {

    private String id;
    private String name;
    private String price;

    public static PartDto of(Part part) {
        PartDto dto = new PartDto();
        dto.setId(String.valueOf(part.getId()));
        dto.setName(part.getName());
        dto.setPrice(part.getPriceString());
        return dto;
    }

    public boolean hasId() {
        return isNotEmpty(id);
    }

    public boolean hasPrice() {
        return isNotEmpty(price);
    }

}
