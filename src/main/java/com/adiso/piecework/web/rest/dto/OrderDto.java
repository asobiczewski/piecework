package com.adiso.piecework.web.rest.dto;

import com.adiso.piecework.domain.managing.order.Order;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@XmlRootElement
public class OrderDto implements Serializable {

    private String id;
    private String name;
    private List<PartDto> parts = new ArrayList<>();

    public static OrderDto of(Order order) {
        OrderDto dto = new OrderDto();
        dto.setId(String.valueOf(order.getId()));
        dto.setName(order.getName());
        dto.setParts(order.getParts().stream()
            .map(PartDto::of)
            .collect(Collectors.toList())
        );
        return dto;
    }

}
