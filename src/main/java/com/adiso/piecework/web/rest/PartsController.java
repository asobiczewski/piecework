package com.adiso.piecework.web.rest;

import com.adiso.piecework.domain.managing.part.Part;
import com.adiso.piecework.domain.managing.part.PartService;
import com.adiso.piecework.web.rest.dto.PartDto;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@Log
@RequestMapping("/managing/parts")
public class PartsController {

    private final PartService partService;

    @Autowired
    public PartsController(PartService partService) {
        this.partService = partService;
    }

    @RequestMapping(method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PartDto> allParts() {
        return partService.getAll().stream()
            .map(PartDto::of)
            .collect(Collectors.toList());
    }

    @RequestMapping(method = RequestMethod.POST,
        consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody PartDto dto) {
        partService.create(Part.of(dto));
    }

    @RequestMapping(method = RequestMethod.PUT,
        consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void update(@RequestBody PartDto dto) {
        partService.update(Part.of(dto));
    }


}
