package com.adiso.piecework.web.rest;

import com.adiso.piecework.domain.managing.order.Order;
import com.adiso.piecework.domain.managing.order.OrderService;
import com.adiso.piecework.web.rest.dto.OrderDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/managing/orders")
public class OrdersController {

    private final OrderService orderService;

    @Autowired
    public OrdersController(OrderService orderService) {
        this.orderService = orderService;
    }

    public List<OrderDto> getAll() {
        return orderService.getAll().stream()
            .map(OrderDto::of)
            .collect(Collectors.toList());
    }

    public HttpStatus create(OrderDto order) {
        orderService.create(Order.of(order));
        return HttpStatus.OK;
    }


}
