package com.adiso.piecework.web.rest.advice;

import lombok.extern.java.Log;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@Log
@ControllerAdvice
public class ExceptionHandlingAdvice {

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseStatus(value = HttpStatus.CONFLICT, reason = "Data integrity violated")
    public void conflict() {
        log.warning("Data integrity violated");
    }

}
