'use strict';

var app = angular.module('pieceworkApp', [
    'ui.bootstrap',
    'ui.router'
    ]
).config(function($stateProvider, $urlRouterProvider, $logProvider){
    $logProvider.debugEnabled(true);
    $urlRouterProvider.otherwise("/home");
    $stateProvider
       .state('home', {
         url: "/home",
         templateUrl: "views/home.html"
       })
       .state('parts', {
         url: "/parts",
         templateUrl: "views/parts.html",
         controller: function($scope) {
           $scope.items = ["A", "List", "Of", "Items"];
         }
       })
       .state('state2', {
         url: "/state2",
         templateUrl: "partials/state2.html"
       })
       .state('state2.list', {
         url: "/list",
         templateUrl: "partials/state2.list.html",
         controller: function($scope) {
           $scope.things = ["A", "Set", "Of", "Things"];
         }
       });
});
