app.service('OrdersService', ['$http', function($http){
    var url = '/api/managing/orders';
    return {
        all : function(){
            return $http.get(url).then(function(response){
                return response.data;
            });
        }
    }
}]);
