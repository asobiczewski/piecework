app.service('PartsService', ['$http', function($http){
    var url = '/api/managing/parts';
    return {
        all: function(){
            return $http.get(url).then(function(response){
                return response.data;
            });
        },

        create: function(newPart){
            $http.post(url, newPart);
        },
        update: function(part){
            $http.put(url, part);
        }
    }
}]);
