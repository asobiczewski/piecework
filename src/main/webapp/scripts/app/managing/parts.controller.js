app.controller('PartsCtrl', ['$scope', 'PartsService', function($scope, PartsService){

    $scope.parts = [];
    $scope.filter = '';
    $scope.editedPartId = 0;
    $scope.newPart = null;

    PartsService.all().then(function(data){
        $scope.parts = data;
    });

    $scope.create = function(newPart){
        $scope.cancelEdit();
        newPart.id = $scope.parts.length;
        $scope.parts.push(newPart);
        newPart.id = null;
        $scope.newPart = null;
        PartsService.create(newPart);
    }

    $scope.edit = function(part){
       $scope.editedPartId = part.id;
    }

    $scope.update = function(part){
        $scope.cancelEdit();
        PartsService.update(part);
    }

    $scope.cancelEdit = function(){
        $scope.editedPartId = 0;
    }

    $scope.add = function(){
        $scope.newPart = {};
    }

    $scope.cancelAdd = function(){
        $scope.newPart = null;
    }

}]);
