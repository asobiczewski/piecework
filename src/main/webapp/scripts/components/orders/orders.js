app.directive('orders', function(){
    return {
        restrict: 'E',
        templateUrl: 'scripts/components/orders/orders.html'
    }
});
