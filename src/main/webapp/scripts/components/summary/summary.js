app.directive('summary', function(){
    return {
        restrict: 'E',
        scope: {
                 money: '@',
                 percent: '@'
        },
        templateUrl: 'scripts/components/summary/summary.html'
    }
});
