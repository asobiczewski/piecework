app.directive('parts', function(){
    return {
        restrict: 'E',
        templateUrl: 'scripts/components/parts/parts.html'
    }
});
